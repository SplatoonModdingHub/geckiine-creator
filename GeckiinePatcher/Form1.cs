﻿using System.Windows.Forms;
using System.IO;
using static System.Convert;
using System.Reflection;

namespace GeckiinePatcher
{
    public partial class genForm : Form
    {
        public genForm()
        {
            InitializeComponent();

        }

        private void textBox1_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void genForm_Load(object sender, System.EventArgs e)
        {

           /*This just loads the IP in the elf*/
            
            if (File.Exists(@".\geckiine.elf"))
            {
                Stream getIP = new FileStream(@".\geckiine.elf", FileMode.Open);
                BinaryReader setbox = new BinaryReader(getIP);

                getIP.Seek(0x5E4E, 0x0);
                byte ip1 = setbox.ReadByte();
                var ip1c = ToInt16(ip1);
                textBox1.Text = ip1c.ToString();

                getIP.Seek(0x5E4F, 0x0);
                byte ip2 = setbox.ReadByte();
                var ip2c = ToInt16(ip2);
                textBox2.Text = ip2.ToString();

                getIP.Seek(0x5E52, 0x0);
                byte ip3 = setbox.ReadByte();
                var ip3c = ToInt16(ip3);
                textBox3.Text = ip3c.ToString();

                getIP.Seek(0x5E53, 0x0);
                byte ip4 = setbox.ReadByte();
                var ip4c = ToInt16(ip4);
                textBox4.Text = ip4c.ToString();
                getIP.Close();
            }
            else
            {

            }
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            int[] IP = new int[4] { ToInt32(textBox1.Text), ToInt32(textBox2.Text), ToInt32(textBox3.Text), ToInt32(textBox4.Text) };
            uint[] UIP = new uint[4] { ToUInt32(IP[0]), ToUInt32(IP[1]), ToUInt32(IP[2]), ToUInt32(IP[3]) };
            byte[] bIP = new byte[6] { ToByte(UIP[0]), ToByte(UIP[1]), 0x61, 0x29, ToByte(UIP[2]), ToByte(UIP[3]) };


            var binasm = Assembly.GetExecutingAssembly();
            Stream geckiine = binasm.GetManifestResourceStream("GeckiinePatcher.geckiine.elf");
            byte[] conv;
            using (var reader = new MemoryStream())
            {
                geckiine.CopyTo(reader);
                geckiine.Close();

                conv = reader.ToArray();
            }


            var ToWrite = new FileStream(@".\geckiine.elf", FileMode.Create, FileAccess.Write);
            BinaryWriter kaku = new BinaryWriter(ToWrite);

            kaku.Write(conv);
            kaku.Seek(0x00005E4E, 0x0);
            kaku.Write(bIP);
            ToWrite.Close();

            MessageBox.Show("Done!");

        }

        private void pictureBox1_Click(object sender, System.EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}
